// 云函数入口文件
const cloud = require("wx-server-sdk");

cloud.init();

// 云函数入口函数
exports.main = async (event, context) => {
  try {
    const wxContext = cloud.getWXContext();

    cloud.updateConfig({
      env: wxContext.ENV
    });

    const db = cloud.database();

    return await db
      .collection("search_histories")
      .where({
        _openid: wxContext.OPENID
      })
      .remove();
  } catch (e) {
    console.error(e);
  }
};
